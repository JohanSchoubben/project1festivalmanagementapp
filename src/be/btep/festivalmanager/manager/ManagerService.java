package be.btep.festivalmanager.manager;

public interface ManagerService {

    void chooseFestival();
    void serveFestival(int index);
    void start();

}
