package be.btep.festivalmanager.manager;

import be.btep.festivalmanager.festival.Act;
import be.btep.festivalmanager.festival.AllAboutFestival;
import be.btep.festivalmanager.festival.Festival;

import static be.btep.festivalmanager.menu.MenuUtility.thinLine;

public class FestivalServiceImpl implements FestivalService{


    @Override
    public void registerNewAct(int chosenFestivalIndex) {

    }

    @Override
    public void registerAct(Act act) {

    }


    @Override
    public void setFestival(Festival festival) {

    }

    @Override
    public void buyTicket(int chosenFestivalIndex) {

    }

    @Override
    public void viewBalanceSheet(int chosenFestivalIndex) {

    }

    /**
     * The method displayLineup() to get the line-up display Acts name, start date, time and duration
     * @param chosenFestivalIndex parameter to track the chosen festival (firstFestival or secondFestival)
     */
    @Override
    public void displayLineUp(int chosenFestivalIndex) {
       if (chosenFestivalIndex==0){
           Act[] festivalOneLineUp = AllAboutFestival.getFirstFestivalActs();
           for (int i = 0; i < festivalOneLineUp.length; i++) {

               System.out.printf("%n%s%n%s%n", thinLine(), thinLine());
               System.out.println(festivalOneLineUp[i].getStartTime().getDayOfMonth() + "-"
                       +festivalOneLineUp[i].getStartTime().getMonth() + "-" +festivalOneLineUp[i].getStartTime().getYear());
               System.out.println(festivalOneLineUp[i].getStartTime().getHour() + "-"+ festivalOneLineUp[i].getStartTime().getMinute());
               System.out.println(festivalOneLineUp[i].getDuration().toString());
               System.out.println(festivalOneLineUp[i].getTalent().getName());


           }

       }else{
           Act[] festivalOneLineUp = AllAboutFestival.getSecondFestivalActs();
           for (int i = 0; i < festivalOneLineUp.length; i++) {

               System.out.printf("%n%s%n%s%n", thinLine(), thinLine());
               System.out.println(festivalOneLineUp[i].getStartTime().getDayOfMonth() + "-" +festivalOneLineUp[i].getStartTime().getMonth() +
                       "-" +festivalOneLineUp[i].getStartTime().getYear());
               System.out.println(festivalOneLineUp[i].getStartTime().getHour() + "-"+ festivalOneLineUp[i].getStartTime().getMinute());
               System.out.println(festivalOneLineUp[i].getDuration().toString());
               System.out.println(festivalOneLineUp[i].getTalent().getName());


           }

       }

    }
}
