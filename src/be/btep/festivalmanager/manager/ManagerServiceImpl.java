package be.btep.festivalmanager.manager;

import be.btep.festivalmanager.festival.AllAboutFestival;
import be.btep.festivalmanager.festival.Festival;

import static be.btep.festivalmanager.menu.KeyboardUtility.askForChoice;
import static be.btep.festivalmanager.menu.MenuUtility.*;

public class ManagerServiceImpl implements ManagerService {

    private Festival[] listOfFestivals = AllAboutFestival.getOurFestivalsNames();
    private FestivalService festivalService=new FestivalServiceImpl();

    /**
     * chooseFestival() to choose festival, calls the list of festivals and integrates the KeyboardUtility
     */

    @Override
    public void chooseFestival() {
        String[] festivalNames = new String[listOfFestivals.length];
        for (int i = 0; i < festivalNames.length; i++) {
            festivalNames[i] = listOfFestivals[i].toString();
            int chosenFestival = askForChoice(festivalNames);
            System.out.printf("%n%s%n%s%n", thickLine(), thickLine());

        }
    }

    /**
     * serveFastival() to provide the list of choices, whether to displayLinUp,to Register new act, to buy tickets or view balance.
     * @param chosenFestivalIndex to track and refer to the chosenFestival
     */
    @Override
    public void serveFestival(int chosenFestivalIndex) {

        System.out.println(thickLine());
        System.out.println(thickLine());
        boolean toContinue = true;
        do {
            int choice = askForChoice(new String[]{
                    "View Line-up",
                    "Register new Act",
                    "Buy tickets",
                    "View balance sheet",
                    "Quit"});

            switch (choice) {
                case 0:
                    festivalService.displayLineUp(chosenFestivalIndex);
                    break;
                case 1:
                    System.out.println(thickLine());
                    System.out.println(center("Register your act"));
                    System.out.println(thinLine());
                    festivalService.registerNewAct(chosenFestivalIndex);
                    System.out.println(thickLine());
                    break;
                case 2:
                    festivalService.buyTicket(chosenFestivalIndex); //buyTicket to be made
                    break;
                case 3:
                    festivalService.viewBalanceSheet(chosenFestivalIndex); //viewBalance to be made

                    break;
                default:
                    toContinue = false;
                    break;

            }
        }while (toContinue) ;

    }

    /**
     * Method start() to get the start of our project including the name of the Project, and the team, then providing the list of festivals.
     */
    public void start() {

        System.out.printf("%n%s%n%s%n%s%n%s%n%s%n",
                center("BTEP presents"), thickLine(),
                center("Festival Manager"), center("by TEAM LATTE"),
                thickLine());

        String[] festivalNames = new String[listOfFestivals.length];

        for (int i = 0; i < listOfFestivals.length; i++) {
            festivalNames[i]=listOfFestivals[i].getName();
        }
/**
 * askForChoice in the KeyboardUtility class sets an index to choose a festival
 */
        int chosenFestivalIndex = askForChoice(festivalNames);

       serveFestival(chosenFestivalIndex);

    }
}

















