package be.btep.festivalmanager.manager;

import be.btep.festivalmanager.festival.Act;
import be.btep.festivalmanager.festival.Festival;

public interface FestivalService{
    void registerNewAct(int chosenFestivalIndex);
    void registerAct(Act act);
    void setFestival(Festival festival);
    void buyTicket(int chosenFestivalIndex);
    void viewBalanceSheet(int chosenFestivalIndex);
    void displayLineUp(int chosenFestivalIndex);



}
