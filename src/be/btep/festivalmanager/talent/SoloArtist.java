package be.btep.festivalmanager.talent;

public class SoloArtist extends Talent {
    private String artist;

    public SoloArtist(String name, String artist) {
        super(name);
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }





}

