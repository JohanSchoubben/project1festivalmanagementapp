package be.btep.festivalmanager.talent;

public class Band extends Talent{
    private String[] members;

    public Band(String name, String[] members) {
        super(name);
        this.members = members;
    }

    public Band(String name) {
        super(name);
    }

    public String[] getMembers() {
        return members;
    }

    public void setMembers(String[] members) {
        this.members = members;
    }
}