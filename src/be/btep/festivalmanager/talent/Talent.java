package be.btep.festivalmanager.talent;

public class Talent {
    private String name;

    public Talent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}