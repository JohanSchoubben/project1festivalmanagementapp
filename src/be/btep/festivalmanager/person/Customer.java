package be.btep.festivalmanager.person;

import java.time.LocalDate;

public class Customer extends Person{
    private Long ID;

    public Customer(String firstName, String lastName, Gender gender, LocalDate dateOfBirth, String address, String email, Long ID) {
        super(firstName, lastName, gender, dateOfBirth, address, email);
        this.ID = ID;
    }
}
