package be.btep.festivalmanager.person;

public enum Gender {
    MALE,
    FEMALE,
    OTHER;

}
