package be.btep.festivalmanager.person;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Employee extends Person{
    private BigDecimal hourlyWage;

    public Employee(String firstName, String lastName, Gender gender, LocalDate dateOfBirth, String address, String email, BigDecimal hourlyWage) {
        super(firstName, lastName, gender, dateOfBirth, address, email);
        this.hourlyWage = hourlyWage;
    }
}

