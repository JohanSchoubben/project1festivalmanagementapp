package be.btep.festivalmanager.person;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Artist extends Employee {
    private String instrument;

    public Artist(String firstName, String lastName, Gender gender, LocalDate dateOfBirth,
                  String address, String email, BigDecimal hourlyWage, String instrument) {

        super(firstName, lastName, gender, dateOfBirth, address, email, hourlyWage);
        this.instrument = instrument;
    }

    public Artist[] getAllArtists() {
        Artist artist1 = new Artist("Mike", "Smith", Gender.FEMALE,
                LocalDate.of(1980, 01, 16),
                "Wommelgem", "mikesmith@hotmail.com",
                BigDecimal.valueOf(100), "Guitar");

        Artist artist2 = new Artist("James", "Love", Gender.MALE,
                LocalDate.of(1990, 02, 20),
                "Amsterdam", "jameslove@outlook.com",
                BigDecimal.valueOf(150), "Piano");

        Artist artist3 = new Artist("Natasha", "Rab", Gender.FEMALE,
                LocalDate.of(1985, 04, 15),
                "Brussels", "natasharab@yahoo.com",
                BigDecimal.valueOf(100), "Guitar");


        Artist[] allArtist = {artist1, artist2,artist3};
        return allArtist;
    }
}


