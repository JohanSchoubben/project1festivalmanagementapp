package be.btep.festivalmanager.festival;

import be.btep.festivalmanager.person.Artist;
import be.btep.festivalmanager.person.Customer;
import be.btep.festivalmanager.person.Gender;
import be.btep.festivalmanager.talent.Band;
import be.btep.festivalmanager.talent.SoloArtist;
import be.btep.festivalmanager.talent.Talent;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * AllAboutFestival is a helping class to be used during to display choices, line-up display
 */
public class AllAboutFestival  {
    /**
     * Method to create lis of festivals to be called during the display
     * @return the list of the festivals
     */
    public static Festival[] getOurFestivalsNames() {
        Festival festival1 = new Festival();
        Festival festival2 = new Festival();

        festival1.setName("Shake it up");
        festival2.setName("Play it music");

        Festival[] ourFestivals = {festival1, festival2};
        return ourFestivals;

    }

    /**
     * Method to create an array of days of the first festival
     * @return festival days of the first festival
     */

    public FestivalDay[] getFirstFestivalDays() {
        FestivalDay festivalDay1 = new FestivalDay(LocalDate.of(2021, 3, 25), LocalTime.of(10, 00));
        FestivalDay festivalDay2 = new FestivalDay(LocalDate.of(2021, 3, 26), LocalTime.of(10, 00));
        FestivalDay festivalDay3 = new FestivalDay(LocalDate.of(2021, 3, 27), LocalTime.of(10, 00));

        FestivalDay[] festivalDays = {festivalDay1, festivalDay2, festivalDay3};
        return festivalDays;
    }

    /**
     * Method to create an array of days of the second festival
     * @return festival days of the second festival
     */
    public FestivalDay[] getSecondFestivalDays() {
        FestivalDay festivalDay1 = new FestivalDay(LocalDate.of(2021, 4, 25), LocalTime.of(10, 00));
        FestivalDay festivalDay2 = new FestivalDay(LocalDate.of(2021, 4, 26), LocalTime.of(10, 00));
        FestivalDay festivalDay3 = new FestivalDay(LocalDate.of(2021, 4, 27), LocalTime.of(10, 00));

        FestivalDay[] festivalDays = {festivalDay1, festivalDay2, festivalDay3};
        return festivalDays;
    }

    /**
     * Method to create an array of talents
     * @return list of talents
     */
    public static Talent[] getTalents() {
        Talent talent1 = new Band("Band Fame"); // Need to make members String in class Band
        Talent talent2 = new Band("Band GotTalent");
        Talent talent3 = new SoloArtist("Sammy Walson", "Solo1");
        Talent talent4 = new SoloArtist("Natasha Loulu ", "Solo2");
        Talent talent5 = new Band("Band SunLight");

        Talent[] talents = {talent1, talent2, talent3, talent4, talent5};
        return talents;
    }

    /**
     * Method to create an array of artists
     * @return list of artists
     */
    public Artist[] getArtists() {
        Artist artist1 = new Artist("Mike", "Smith", Gender.MALE, LocalDate.of(1980, 01, 16), "Wommelgem", "mikesmith@hotmail.com", BigDecimal.valueOf(100), "Guitar");
        Artist artist2 = new Artist("James", "Love", Gender.MALE, LocalDate.of(1990, 02, 20), "Amsterdam", "jameslove@outlook.com", BigDecimal.valueOf(150), "Piano");
        Artist artist3 = new Artist("Natasha", "Rab", Gender.FEMALE, LocalDate.of(1985, 04, 15), "Brussels", "natasharab@yahoo.com", BigDecimal.valueOf(100), "Guitar");
        Artist artist4 = new Artist("Nathan", "Bob", Gender.MALE, LocalDate.of(1970, 03, 25), "Leuven", "nathanbb@telenet.be", BigDecimal.valueOf(200), "Guitar");
        Artist artist5 = new Artist("Loulu", "Safin", Gender.FEMALE, LocalDate.of(1975, 02, 10), "Leuven", "nathanbb@telenet.be", BigDecimal.valueOf(200), "Guitar");

        Artist[] artists = {artist1, artist2, artist3, artist4, artist5};
        return artists;
    }

    public Customer[] getCustomers(){
        Customer customer1 = new Customer("Kamal","Jamil",Gender.MALE,
                LocalDate.of(1978,02,16),"Amsterdam","yahoo",2L);
        Customer[] customers = {customer1};
        return customers;
    }

    /**
     * Method to create and array of the first festival acts
     * @return list of acts
     */
    public static Act[] getFirstFestivalActs() {
        LocalDateTime dateAndTime = LocalDateTime.of(2021, 3, 25, 10, 00);
        Act act1 = new Act(LocalDateTime.of(2021, 03, 25,14, 30), Duration.ofMinutes(120), getTalents()[0]);
        Act act2 = new Act(LocalDateTime.of(2021, 03, 25,18, 00), Duration.ofMinutes(120), getTalents()[2]);
        Act act3 = new Act(LocalDateTime.of(2021, 03, 26,18, 00), Duration.ofMinutes(60), getTalents()[3]);
        Act act4 = new Act(LocalDateTime.of(2021, 03, 27, 18, 00), Duration.ofMinutes(120), getTalents()[4]);

        Act[] acts = {act1, act2, act3, act4};
        return acts;
    }
    /**
     * Method to create and array of the second festival acts
     * @return list of acts
     */
    public static Act[] getSecondFestivalActs() {

        Act act1 = new Act(LocalDateTime.of(2021, 04, 25, 14, 30), Duration.ofMinutes(120), getTalents()[0]);
        Act act2 = new Act(LocalDateTime.of(2021, 04, 25, 18, 00), Duration.ofMinutes(180), getTalents()[2]);
        Act act3 = new Act(LocalDateTime.of(2021, 04, 26, 18, 00), Duration.ofMinutes(60), getTalents()[3]);
        Act act4 = new Act(LocalDateTime.of(2021, 04, 27, 18, 00), Duration.ofMinutes(120), getTalents()[4]);

        Act[] acts = {act1, act2, act3, act4};
        return acts;
    }

    }

