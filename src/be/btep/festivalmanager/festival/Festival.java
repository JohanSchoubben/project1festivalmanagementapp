package be.btep.festivalmanager.festival;

import java.math.BigDecimal;

public class Festival {
    private String name;
    private FestivalDay[] days;
    private Venue venue;
    private BigDecimal dayPrice;
    private String genre;

    /**
     * Festival() to create festival name, day, venue, day price and genre
      * @param name name of festival
     * @param days
     * @param venue
     * @param dayPrice
     * @param genre
     */
    public Festival(String name, FestivalDay[] days, Venue venue, BigDecimal dayPrice, String genre) {
        this.name = name;
        this.days = days;
        this.venue = venue;
        this.dayPrice = dayPrice;
        this.genre = genre;
    }

    public Festival() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FestivalDay[] getDays() {
        return days;
    }

    public void setDays(FestivalDay[] days) {
        this.days = days;
    }

    public be.btep.festivalmanager.festival.Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public BigDecimal getDayPrice() {
        return dayPrice;
    }

    public void setDayPrice(BigDecimal dayPrice) {
        this.dayPrice = dayPrice;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
