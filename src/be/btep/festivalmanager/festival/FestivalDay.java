package be.btep.festivalmanager.festival;

import java.time.LocalDate;
import java.time.LocalTime;

public class FestivalDay extends FestivalDate {
        private int ticketsLeft;
        private Act[] lineUp;
        private String name;

        public FestivalDay(int day, int month, int year, int hours, int minutes, int ticketsLeft) {
            super(day, month, year, hours, minutes);
            this.ticketsLeft = ticketsLeft;

        }

        public FestivalDay(int day, int month, int year, int ticketsLeft, Act[] lineUp) {
            super(day, month, year);
            this.ticketsLeft = ticketsLeft;
            this.lineUp = lineUp;
        }


        public FestivalDay(int day, int month, int year, int hours, int minutes, int ticketsLeft, Act[] lineUp) {
            super(day, month, year, hours, minutes);
            this.ticketsLeft = ticketsLeft;
            this.lineUp = lineUp;
        }

            public FestivalDay(LocalDate localDate, LocalTime localTime) {
            super(localDate, localTime);

        }

        public int getTicketsLeft() {
            return ticketsLeft;
        }

        public void setTicketsLeft(int ticketsLeft) {
            this.ticketsLeft = ticketsLeft;
        }

        public Act[] getLineUp() {
            return lineUp;
        }

        public void setLineUp(Act[] lineUp) {
            this.lineUp = lineUp;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }




