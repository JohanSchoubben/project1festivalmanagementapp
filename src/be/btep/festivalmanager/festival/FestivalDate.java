package be.btep.festivalmanager.festival;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * class FestivalDate is created to be used by FestivalDay class
 */
public class FestivalDate {
    private int day;
    private int month;
    private int year;
    private int hours;
    private int minutes;
    private LocalDate localDate;
    private LocalTime localTime;

    public FestivalDate(int day, int month, int year, int hours, int minutes) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hours = hours;
        this.minutes = minutes;
    }
    public FestivalDate( int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public FestivalDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    /**
     * FestivalDate() to create local dates and times of each festival
     * @param localDate
     * @param localTime
     */
    public FestivalDate(LocalDate localDate, LocalTime localTime){
        this.localDate = localDate;
        this.localTime = localTime;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int[] createFestivalDateArray() {
        int[] festivalDateArray = {day, month, year};
        return festivalDateArray;
    }

    public int[] createFestivalTimeArray() {
        int[] festivalTimeArray = {hours, minutes};
        return festivalTimeArray;
    }
}

