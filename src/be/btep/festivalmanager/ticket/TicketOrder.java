package be.btep.festivalmanager.ticket;

import be.btep.festivalmanager.person.Customer;

import java.time.LocalDate;

import be.btep.festivalmanager.person.Gender;

public class TicketOrder extends Customer {
    private Customer customer;
    private TicketOrder[] tickets;

    public TicketOrder(String firstName, String lastName, Gender gender, LocalDate dateOfBirth, String address, String email, Long ID, Customer customer, TicketOrder[] tickets) {
        super(firstName, lastName, gender, dateOfBirth, address, email, ID);
        this.customer = customer;
        this.tickets = tickets;
    }
}

